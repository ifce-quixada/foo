terraform {
  required_version = "1.0.0"
  backend "http" {}

  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "2.5.1"
    }
  }
}
