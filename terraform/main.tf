# helm release
resource "helm_release" "netbox" {
  namespace        = "foo" 
  name = "foo"
  create_namespace = true
  chart            = "../helm-chart/" 
  cleanup_on_fail  = true
  atomic           = true

  set {
    name = "image.tag"
    value = var.app_version
  }
  
}
